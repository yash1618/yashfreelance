﻿using JewelApi.Bal;
using JewelApi.Base;
using JewelApi.Contract;
using JewelApi.Contract.Home;
using System;
using System.Globalization;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace JewelApi.Controllers
{
    public class LoginController : ApiController
    {
        LoginBal loginbal;
        public LoginController()
        {
            loginbal = new LoginBal();
        }

        [HttpPost]
        public LoginResponse Index(LoginRequest request)
        {
            var res = new LoginResponse();
            try
            {
                var data = loginbal.Login(request);
                if (data.Id != 0)
                {
                    res.User = data;
                    res.Message = "Successful";
                    res.Status = Base.Status.Success;
                    res.StatusCode = Base.StatusCode.Success1;
                    return res;
                }
                else
                {
                    res.Message = "No Data Received";
                    res.Status = Base.Status.Failure;
                    res.StatusCode = Base.StatusCode.Failure1;
                    return res;
                }
            }
            catch (Exception e)
            {
                res.Message = e.Message.ToString();
                res.Status = Base.Status.Failure;
                res.StatusCode = Base.StatusCode.Failure2;
                return res;
            }
        }

        [HttpPost]
        public VerifyPhoneResponse VerifyPhone(VerifyPhoneRequest request)
        {
            var res = new VerifyPhoneResponse();
            try
            {
                var data = loginbal.VerifyPhone(request);
                if (data != null)
                {
                    res.SecurityQuestion = data;
                    res.Message = "Successful";
                    res.Status = Base.Status.Success;
                    res.StatusCode = Base.StatusCode.Success1;
                    return res;
                }
                else
                {
                    res.Message = "Phone No Doesn't Exist";
                    res.Status = Base.Status.Failure;
                    res.StatusCode = Base.StatusCode.Failure1;
                    return res;
                }
            }
            catch (Exception e)
            {
                res.Message = e.Message.ToString();
                res.Status = Base.Status.Failure;
                res.StatusCode = Base.StatusCode.Failure2;
                return res;
            }
        }

        [HttpPost]
        public BaseResponse CreateNewPassword(CreatePassRequest request)
        {
            var res = new BaseResponse();
            try
            {
                var data = loginbal.CreateNewPass(request);
                if (data != 0)
                {
                    res.Message = "Successful";
                    res.Status = Base.Status.Success;
                    res.StatusCode = Base.StatusCode.Success1;
                    return res;
                }
                else
                {
                    res.Message = "Password Not Updated";
                    res.Status = Base.Status.Failure;
                    res.StatusCode = Base.StatusCode.Failure1;
                    return res;
                }
            }
            catch (Exception e)
            {
                res.Message = e.Message.ToString();
                res.Status = Base.Status.Failure;
                res.StatusCode = Base.StatusCode.Failure2;
                return res;
            }
        }

        [HttpGet]
        public MainScreenResponse GetHomeScreen(int id)
        {
            var res = new MainScreenResponse();
            try
            {
                var data = loginbal.GetHomeScreen(id);
                if (data != null)
                {
                    res.model = data;
                    res.Message = "Successful";
                    res.Status = Base.Status.Success;
                    res.StatusCode = Base.StatusCode.Success1;
                    return res;
                }
                else
                {
                    res.Message = "Failed to Retrive Data";
                    res.Status = Base.Status.Failure;
                    res.StatusCode = Base.StatusCode.Failure1;
                    return res;
                }
            }
            catch (Exception e)
            {
                res.Message = e.Message.ToString();
                res.Status = Base.Status.Failure;
                res.StatusCode = Base.StatusCode.Failure2;
                return res;
            }
        }

        [HttpPost]
        public SignUpResponse SignUp(SignUpRequest request)
        {
            var res = new SignUpResponse();
            try
            {
                var data = loginbal.SignUp(request);
                if (data.Id != 0)
                {
                    res.User = data;
                    res.Message = "Successful";
                    res.Status = Base.Status.Success;
                    res.StatusCode = Base.StatusCode.Success1;
                    return res;
                }
                else
                {
                    res.Message = "Couldn't Create User";
                    res.Status = Base.Status.Failure;
                    res.StatusCode = Base.StatusCode.Failure1;
                    return res;
                }
            }
            catch (Exception e)
            {
                res.Message = e.Message.ToString();
                res.Status = Base.Status.Failure;
                res.StatusCode = Base.StatusCode.Failure2;
                return res;
            }
        }

        [HttpGet]
        public GetReportResponse GetAllReports(int id)
        {
            var res = new GetReportResponse();
            try
            {
                var data = loginbal.GetAllReports(id);
                if (data != null)
                {
                    res.Reports = data;
                    res.Message = "Successful";
                    res.Status = Status.Success;
                    res.StatusCode = Base.StatusCode.Success1;
                    return res;
                }
                else
                {
                    res.Message = "Failed to Retrive Data";
                    res.Status = Status.Failure;
                    res.StatusCode = Base.StatusCode.Failure1;
                    return res;
                }
            }
            catch (Exception e)
            {
                res.Message = e.Message.ToString();
                res.Status = Status.Failure;
                res.StatusCode = Base.StatusCode.Failure2;
                return res;
            }
        }

        [HttpGet]
        public GetSingleOrderResponse GetSingleOrderDetails(int id)
        {
            var res = new GetSingleOrderResponse();
            try
            {
                var data = loginbal.GetSingeOrder(id);
                if (data != null)
                {
                    res.SingleOrder = data;
                    res.Message = "Successful";
                    res.Status = Status.Success;
                    res.StatusCode = Base.StatusCode.Success1;
                    return res;
                }
                else
                {
                    res.Message = "Failed to Retrive Data";
                    res.Status = Status.Failure;
                    res.StatusCode = Base.StatusCode.Failure1;
                    return res;
                }
            }
            catch (Exception e)
            {
                res.Message = e.Message.ToString();
                res.Status = Status.Failure;
                res.StatusCode = Base.StatusCode.Failure2;
                return res;
            }
        }

        [HttpPost]
        public BaseResponse ChangeOrderStatus(int id)
        {
            var res = new BaseResponse();
            try
            {
                loginbal.ChangeOrderStatus(id);
                res.Message = "Successful";
                res.Status = Base.Status.Success;
                res.StatusCode = Base.StatusCode.Success1;
                return res;

            }
            catch (Exception e)
            {
                res.Message = e.Message.ToString();
                res.Status = Base.Status.Failure;
                res.StatusCode = Base.StatusCode.Failure2;
                return res;
            }
        }

        [HttpPost]
        public BaseResponse CreateNewOrder()
        {
            var res = new BaseResponse();
            try
            {
                var req = HttpContext.Current.Request;
                var title = req.Form["Title"];
                var desc = req.Form["Description"];
                var Userid = Convert.ToInt32(req.Form["UserId"]);
                var Image = req.Files["Image"];
                //var _path = req.Url.Authority + "/Images/" + Image.FileName;
                var _path = HttpContext.Current.Server.MapPath("~/Images/" + Image.FileName);

                Image.SaveAs(_path);
                loginbal.CreateNewOrder(new CreateOrderRequest { Title = title, Description = desc, UserId = Userid, Image = Image }, _path);
                res.Message = "Successful";
                res.Status = Base.Status.Success;
                res.StatusCode = Base.StatusCode.Success1;
                return res;
            }
            catch (Exception e)
            {
                res.Message = e.Message.ToString();
                res.Status = Base.Status.Failure;
                res.StatusCode = Base.StatusCode.Failure2;
                return res;
            }
        }
    }
}

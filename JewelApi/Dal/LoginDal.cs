﻿using JewelApi.Contract;
using MySql.Data.MySqlClient;
using System;
using System.Configuration;
using System.Linq;
using JewelApi.Contract.Home;
using System.Collections.Generic;
using JewelApi.Models;

namespace JewelApi.Dal
{
    public class LoginDal
    {
        public string connectionString = ConfigurationManager.ConnectionStrings["DatabaseContext"].ConnectionString;

        public User Login(string Mobileno, string Password)
        {
            using (MySqlConnection con = new MySqlConnection(connectionString))
            {
                using (DatabaseContext db = new DatabaseContext(con, true))
                {
                    try
                    {
                        if (db.User.Any(x => x.MobileNo == Mobileno && x.Password == Password))
                        {
                            var user = db.User.Where(x => x.MobileNo == Mobileno && x.Password == Password).FirstOrDefault();
                            return user;
                        }
                        throw new ArgumentNullException("No User Found");
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Database Connection Error");
                    }
                    finally
                    {
                        db.Dispose();
                    }
                }
            }
        }

        internal int CreateNewPass(int userId, string password)
        {
            using (MySqlConnection con = new MySqlConnection(connectionString))
            {
                using (DatabaseContext db = new DatabaseContext(con, true))
                {
                    try
                    {
                        if (db.User.Any(x => x.Id == userId))
                        {
                            var user = db.User.FirstOrDefault(x => x.Id == userId);
                            user.Password = password;
                            db.SaveChanges();
                            return user.Id;
                        }
                        throw new ArgumentNullException("No User Found");
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Database Connection Error");
                    }
                    finally
                    {
                        db.Dispose();
                    }
                }
            }
        }

        internal SingleOrderModel GetSingleOrder(int orderId)
        {
            using (MySqlConnection con = new MySqlConnection(connectionString))
            {
                using (DatabaseContext db = new DatabaseContext(con, true))
                {
                    try
                    {
                        if (db.Order.Any(x => x.Id == orderId))
                        {
                            var data = db.Order.Where(x => x.Id == orderId).Select(x => new SingleOrderModel
                            {
                                Id = x.Id,
                                DateOfOrder = x.DateOfOrder,
                                Description = x.Product.Description,
                                Title = x.Product.Title,
                                ImageUrl = x.Product.ImageUrl,
                                MobileNo = x.User.MobileNo,
                                Status = x.Status
                            }).FirstOrDefault();
                            return data;
                        }
                        throw new Exception("Order Not Found");
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Database Connection Error");
                    }
                    finally
                    {
                        db.Dispose();
                    }
                }
            }
        }

        internal void CreateOrder(int userId, int productid)
        {
            using (MySqlConnection con = new MySqlConnection(connectionString))
            {
                using (DatabaseContext db = new DatabaseContext(con, true))
                {
                    try
                    {
                        var order = new Order {
                            UserId = userId,
                            ProductId = productid,
                            DateOfOrder = DateTime.Now,
                            Status = 0,
                            Time = DateTime.Now
                        };
                        db.Order.Add(order);
                        db.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Database Connection Error");
                    }
                    finally
                    {
                        db.Dispose();
                    }
                }
            }
        }

        internal int InsertProduct(CreateOrderRequest request, string _path)
        {
            using (MySqlConnection con = new MySqlConnection(connectionString))
            {
                using (DatabaseContext db = new DatabaseContext(con, true))
                {
                    try
                    {
                        var prod = new Product
                        {
                            Title = request.Title,
                            Description = request.Description,
                            Status = 1,
                            Time = DateTime.Now,
                            ImageUrl = _path
                        };
                        db.Product.Add(prod);
                        db.SaveChanges();
                        return prod.Id;
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Database Connection Error");
                    }
                    finally
                    {
                        db.Dispose();
                    }
                }
            }
        }

        internal void ChangeOrderStatus(int id)
        {
            using (MySqlConnection con = new MySqlConnection(connectionString))
            {
                using (DatabaseContext db = new DatabaseContext(con, true))
                {
                    try
                    {
                        if (db.Order.Any(x => x.Id == id))
                        {
                            var data = db.Order.Where(x => x.Id == id).FirstOrDefault();
                            if (data.Status == 1)
                            {
                                data.Status = 0;
                                db.SaveChanges();
                            }
                            else
                            {
                                data.Status = 1;
                                db.SaveChanges();
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Database Connection Error");
                    }
                    finally
                    {
                        db.Dispose();
                    }
                }
            }
        }

        internal List<ReportModel> GetAllReports(int userId)
        {
            using (MySqlConnection con = new MySqlConnection(connectionString))
            {
                using (DatabaseContext db = new DatabaseContext(con, true))
                {
                    try
                    {
                        var user = db.User.FirstOrDefault(x => x.Id == userId);
                        if (user.Password.ToLower().Equals("admin11"))
                        {
                            var data = db.Order.OrderByDescending(x => x.DateOfOrder).Select(x => new ReportModel
                            {
                                Title = x.Product.Title,
                                ImageUrl = x.Product.ImageUrl,
                                Id = x.Id,
                                Status = x.Status,
                                DateOfOrder = x.DateOfOrder
                            }).ToList();
                            return data;
                        }
                        else
                        {
                            var data = db.Order.OrderByDescending(x => x.DateOfOrder).Where(x => x.UserId == userId).Select(x => new ReportModel
                            {
                                Title = x.Product.Title,
                                ImageUrl = x.Product.ImageUrl,
                                Id = x.Id,
                                Status = x.Status,
                                DateOfOrder = x.DateOfOrder
                            }).ToList();
                            return data;
                        }
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Database Connection Error");
                    }
                    finally
                    {
                        db.Dispose();
                    }
                }
            }
        }

        internal List<PendingProductModel> GetHomeScreen(int userId)
        {
            using (MySqlConnection con = new MySqlConnection(connectionString))
            {
                using (DatabaseContext db = new DatabaseContext(con, true))
                {
                    try
                    {
                        var user = db.User.FirstOrDefault(x => x.Id == userId);
                        if (user.Password.ToLower().Equals("admin11"))
                        {
                            var data = db.Order.OrderByDescending(x => x.DateOfOrder).Where(x => x.Status == 0).Select(x => new PendingProductModel
                            {
                                Title = x.Product.Title,
                                MobileNo = user.MobileNo,
                                ImageUrl = x.Product.ImageUrl,
                                Id = x.Id
                            }).ToList();
                            return data;
                        }
                        else
                        {
                            var data = db.Order.OrderByDescending(x => x.DateOfOrder).Where(x => x.UserId == userId && x.Status == 0).Select(x => new PendingProductModel
                            {
                                Title = x.Product.Title,
                                MobileNo = user.MobileNo,
                                ImageUrl = x.Product.ImageUrl,
                                Id = x.Id
                            }).ToList();
                            return data;
                        }
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Database Connection Error");
                    }
                    finally
                    {
                        db.Dispose();
                    }
                }
            }
        }

        internal User SignUp(string firstName, string lastName, string mobileNo, string address, string password, string securityQuestion, string answer)
        {
            using (MySqlConnection con = new MySqlConnection(connectionString))
            {
                using (DatabaseContext db = new DatabaseContext(con, true))
                {
                    try
                    {
                        if (!db.User.Any(x => x.MobileNo == mobileNo))
                        {

                            var user = new User
                            {
                                FirstName = firstName,
                                LastName = lastName,
                                Address = address,
                                MobileNo = mobileNo,
                                Password = password,
                                SecurityQuestion = securityQuestion,
                                Answer = answer
                            };
                            db.User.Add(user);
                            db.SaveChanges();
                            return user;
                        }
                        else
                        {
                            throw new Exception("User Already Exists");
                        }

                    }
                    catch (Exception e)
                    {
                        throw new Exception("Database Connection Error");
                    }
                    finally
                    {
                        db.Dispose();
                    }
                }
            }
        }

        public SecurityQues VerifyPhone(string phoneNo)
        {
            using (MySqlConnection con = new MySqlConnection(connectionString))
            {
                using (DatabaseContext db = new DatabaseContext(con, true))
                {
                    try
                    {
                        if (db.User.Any(x => x.MobileNo == phoneNo))
                        {
                            var user = db.User.Where(x => x.MobileNo == phoneNo).FirstOrDefault();
                            return new SecurityQues { SecurityQuestion = user.SecurityQuestion, Answer = user.Answer };
                        }
                        throw new ArgumentNullException("No User Found");
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Database Connection Error");
                    }
                    finally
                    {
                        db.Dispose();
                    }
                }
            }
        }
    }
}
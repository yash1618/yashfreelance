﻿using JewelApi.Base;
using JewelApi.Contract.Home;
using System.Collections.Generic;

namespace JewelApi.Contract
{
    public class MainScreenResponse : BaseResponse
    {
        public List<PendingProductModel> model { get; set; }
    }
}
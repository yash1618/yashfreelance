﻿using JewelApi.Base;

namespace JewelApi.Contract
{
    public class VerifyPhoneRequest : ValidationBase
    {
        public string PhoneNo { get; set; }

        public override string Validate()
        {
            BussinessRuleFailure.Clear();
            if (string.IsNullOrEmpty(PhoneNo))
            {
                BussinessRuleFailure.Add(new BusinessRuleMessage { Message = "Phone No Empty", PropertyName = "PhoneNo" });
            }
            return BussinessRuleFailure.ToString();
        }
    }
}
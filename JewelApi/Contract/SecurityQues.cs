﻿namespace JewelApi.Contract
{
    public class SecurityQues
    {
        public string SecurityQuestion { get; set; }
        public string Answer { get; set; }
    }
}
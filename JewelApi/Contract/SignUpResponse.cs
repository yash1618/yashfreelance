﻿using JewelApi.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JewelApi.Contract
{
    public class SignUpResponse : BaseResponse
    {
        public User User { get; set; }

    }
}
﻿using System;
using JewelApi.Base;

namespace JewelApi.Contract
{
    public class MainScreenRequest : ValidationBase
    {
        public int UserId { get; set; }

        public override string Validate()
        {
            BussinessRuleFailure.Clear();
            if (UserId == 0)
            {
                BussinessRuleFailure.Add(new BusinessRuleMessage { Message = "UserId Empty", PropertyName = "UserId" });
            }
            return BussinessRuleFailure.ToString();
        }
    }
}
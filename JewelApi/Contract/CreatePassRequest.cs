﻿using JewelApi.Base;

namespace JewelApi.Contract
{
    public class CreatePassRequest : ValidationBase
    {
        public int UserId { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }

        public override string Validate()
        {
            BussinessRuleFailure.Clear();
            if (string.IsNullOrEmpty(Password) || string.IsNullOrEmpty(ConfirmPassword))
            {
                BussinessRuleFailure.Add(new BusinessRuleMessage { Message = "Password Empty", PropertyName = "Password" });
            }
            if (!Password.Equals(ConfirmPassword))
            {
                BussinessRuleFailure.Add(new BusinessRuleMessage { Message = "Password Doesn't Match", PropertyName = "Password" });
            }
            return BussinessRuleFailure.ToString();
        }
    }
}
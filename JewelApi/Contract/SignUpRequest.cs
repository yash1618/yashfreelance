﻿using JewelApi.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JewelApi.Contract
{
    public class SignUpRequest : ValidationBase
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string MobileNo { get; set; }
        public string Password { get; set; }
        public string SecurityQuestion { get; set; }
        public string Answer { get; set; }

        public override string Validate()
        {
            BussinessRuleFailure.Clear();
            if (string.IsNullOrEmpty(FirstName))
            {
                BussinessRuleFailure.Add(new BusinessRuleMessage { Message = "FirstName empty", PropertyName = "FirstName" });
            }
            if (string.IsNullOrEmpty(LastName))
            {
                BussinessRuleFailure.Add(new BusinessRuleMessage { Message = "LastName empty", PropertyName = "LastName" });
            }
            if (string.IsNullOrEmpty(Address))
            {
                BussinessRuleFailure.Add(new BusinessRuleMessage { Message = "Address empty", PropertyName = "Address" });
            }
            if (string.IsNullOrEmpty(MobileNo))
            {
                BussinessRuleFailure.Add(new BusinessRuleMessage { Message = "Mobile No empty", PropertyName = "MobileNo" });
            }
            if (string.IsNullOrEmpty(Password))
            {
                BussinessRuleFailure.Add(new BusinessRuleMessage { Message = "Password empty", PropertyName = "Password" });
            }
            if (string.IsNullOrEmpty(SecurityQuestion))
            {
                BussinessRuleFailure.Add(new BusinessRuleMessage { Message = "Security Question empty", PropertyName = "SecurityQuestion" });
            }
            if (string.IsNullOrEmpty(Answer))
            {
                BussinessRuleFailure.Add(new BusinessRuleMessage { Message = "Answer empty", PropertyName = "Answer" });
            }
            return BussinessRuleFailure.ToString();
        }
    }
}
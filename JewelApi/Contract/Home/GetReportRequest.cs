﻿using JewelApi.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JewelApi.Contract.Home
{
    public class GetReportRequest : ValidationBase
    {
        public int UserId { get; set; }

        public override string Validate()
        {
            BussinessRuleFailure.Clear();
            if (UserId == 0)
            {
                BussinessRuleFailure.Add(new BusinessRuleMessage { Message = "UserId Empty", PropertyName = "UserId" });
            }
            return BussinessRuleFailure.ToString();
        }
    }
}
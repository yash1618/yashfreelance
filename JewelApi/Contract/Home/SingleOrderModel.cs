﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JewelApi.Contract.Home
{
    public class SingleOrderModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public DateTime DateOfOrder { get; set; }
        public string MobileNo { get; set; }
        public int Status { get; set; }
    }
}
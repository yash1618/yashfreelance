﻿using JewelApi.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JewelApi.Contract.Home
{
    public class GetSingleOrderResponse : BaseResponse
    {
        public SingleOrderModel SingleOrder { get; set; }
    }
}
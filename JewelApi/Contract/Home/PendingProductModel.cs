﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JewelApi.Contract.Home
{
    public class PendingProductModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ImageUrl { get; set; }
        public string MobileNo { get; set; }
    }
}
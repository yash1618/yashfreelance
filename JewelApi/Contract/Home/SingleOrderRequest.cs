﻿using JewelApi.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JewelApi.Contract.Home
{
    public class SingleOrderRequest : ValidationBase
    {
        public int OrderId { get; set; }

        public override string Validate()
        {
            BussinessRuleFailure.Clear();
            if (OrderId == 0)
            {
                BussinessRuleFailure.Add(new BusinessRuleMessage { Message = "OrderId Empty", PropertyName = "OrderId" });
            }
            return BussinessRuleFailure.ToString();
        }
    }
}
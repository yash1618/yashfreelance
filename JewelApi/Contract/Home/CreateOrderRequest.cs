﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JewelApi.Contract.Home
{
    public class CreateOrderRequest
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public int UserId { get; set; }
        public HttpPostedFile Image { get; set; }
    }
}
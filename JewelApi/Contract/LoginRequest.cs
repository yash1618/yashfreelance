﻿using JewelApi.Base;

namespace JewelApi.Contract
{
    public class LoginRequest : ValidationBase
    {
        public string MobileNo { get; set; }
        public string Password { get; set; }

        public override string Validate()
        {
            BussinessRuleFailure.Clear();
            if (string.IsNullOrEmpty(MobileNo))
            {
                BussinessRuleFailure.Add(new BusinessRuleMessage { Message = "Mobile No Empty", PropertyName = "MobileNo" });
            }
            if (string.IsNullOrEmpty(Password))
            {
                BussinessRuleFailure.Add(new BusinessRuleMessage { Message = "Password Empty", PropertyName = "Password" });
            }
            return BussinessRuleFailure.ToString();
        }
    }
}
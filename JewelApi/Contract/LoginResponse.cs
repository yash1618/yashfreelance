﻿using JewelApi.Base;

namespace JewelApi.Contract
{
    public class LoginResponse : BaseResponse
    {
        public User User { get; set; }
    }
}
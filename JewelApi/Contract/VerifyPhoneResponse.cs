﻿using JewelApi.Base;

namespace JewelApi.Contract
{
    public class VerifyPhoneResponse : BaseResponse
    {
        public SecurityQues SecurityQuestion { get; set; }
    }
}
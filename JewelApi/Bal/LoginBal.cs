﻿using JewelApi.Contract;
using JewelApi.Contract.Home;
using JewelApi.Dal;
using System;
using System.Collections.Generic;

namespace JewelApi.Bal
{
    public class LoginBal
    {
        LoginDal logindal;

        public LoginBal()
        {
            logindal = new LoginDal();
        }

        public User Login(LoginRequest request)
        {
            var res = request.Validate();
            if (string.IsNullOrEmpty(res))
            {
                return logindal.Login(request.MobileNo, request.Password);
            }
            throw new Exception(res);
        }

        public SecurityQues VerifyPhone(VerifyPhoneRequest request)
        {
            var res = request.Validate();
            if (string.IsNullOrEmpty(res))
            {
                return logindal.VerifyPhone(request.PhoneNo);
            }
            throw new Exception(res);
        }

        public int CreateNewPass(CreatePassRequest request)
        {
            var res = request.Validate();
            if (string.IsNullOrEmpty(res))
            {
                return logindal.CreateNewPass(request.UserId,request.Password);
            }
            throw new Exception(res);
        }

        internal User SignUp(SignUpRequest request)
        {
            var res = request.Validate();
            if (string.IsNullOrEmpty(res))
            {
                return logindal.SignUp(request.FirstName,request.LastName,request.Address,request.MobileNo,request.Password,request.SecurityQuestion,request.Answer);
            }
            throw new Exception(res);
        }

        internal List<PendingProductModel> GetHomeScreen(int id)
        {
            
            if (id != 0)
            {
                return logindal.GetHomeScreen(id);
            }
            throw new Exception("UserId Empty");
        }

        internal List<ReportModel> GetAllReports(int id)
        {
            if (id != 0)
            {
                return logindal.GetAllReports(id);
            }
            throw new Exception("UserId Empty");
        }

        internal SingleOrderModel GetSingeOrder(int id)
        {
            if (id != 0)
            {
                return logindal.GetSingleOrder(id);
            }
            throw new Exception("OrderId Empty");
        }

        internal void ChangeOrderStatus(int id)
        {
            if (id != 0)
            {
                logindal.ChangeOrderStatus(id);
            }
            throw new Exception("OrderId Empty");
        }

        internal void CreateNewOrder(CreateOrderRequest request, string _path)
        {
            var productid = logindal.InsertProduct(request, _path);
            logindal.CreateOrder(request.UserId, productid);
        }
    }
}
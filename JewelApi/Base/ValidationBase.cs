﻿namespace JewelApi.Base
{
    public abstract class ValidationBase
    {
        public ValidationBase()
        {
            BussinessRuleFailure = new BusinessRuleMessages();
        }
        public BusinessRuleMessages BussinessRuleFailure { get; set; }
        public abstract string Validate();
    }
}

﻿using System;

namespace JewelApi.Base
{
    public class BaseResponse
    {

        public Status Status { get; set; }
        public StatusCode StatusCode { get; set; }
        public String Message { get; set; }
        public String ReadableMessage { get; set; }

        public static BaseResponse GetSuccessResponse(StatusCode statusCode = StatusCode.Success1)
        {
            return new BaseResponse { Status = Status.Success, StatusCode = statusCode };
        }

        public static BaseResponse GetFailureResponse(String Message, StatusCode statusCode = StatusCode.Failure1)
        {
            return new BaseResponse { Status = Status.Failure, StatusCode = statusCode, Message = Message };
        }
    }

    public enum Status
    {
        None,
        Success,
        Failure
    }
    public enum StatusCode
    {
        Success1,
        Failure1,
        Failure2,
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JewelApi.Base
{
    public class BusinessRuleMessages : List<BusinessRuleMessage>
    {
        public override string ToString()
        {
            return string.Join(Environment.NewLine, this.Select(x => x.Message));
        }
    }

    public class BusinessRuleMessage
    {
        public string PropertyName { get; set; }
        public string Message { get; set; }
    }
}